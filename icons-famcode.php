<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              cloud3dots.com
 * @since             0.1.0
 * @package           Icons_Famcode
 *
 * @wordpress-plugin
 * Plugin Name:       Icons FAMCode
 * Plugin URI:        https://gitlab.com/cloud3dots/wp-plugin-icons-famcode
 * Description:       FAMCode Font Icons for WordPress.
 * Version:           0.1.0
 * Author:            cloud3dots
 * Author URI:        cloud3dots.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       icons-famcode
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (! defined('WPINC')) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 0.1.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('ICONS_FAMCODE_VERSION', '0.1.0');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-icons-famcode-activator.php
 */
function activate_icons_famcode()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-icons-famcode-activator.php';
    Icons_Famcode_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-icons-famcode-deactivator.php
 */
function deactivate_icons_famcode()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-icons-famcode-deactivator.php';
    Icons_Famcode_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_icons_famcode');
register_deactivation_hook(__FILE__, 'deactivate_icons_famcode');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-icons-famcode.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.1.0
 */
function run_icons_famcode()
{
    $plugin = new Icons_Famcode();
    $plugin->run();
}
run_icons_famcode();
