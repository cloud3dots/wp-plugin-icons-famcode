=== Icons FAMCode ===
Contributors: cloud3dots, jfederico
Donate link: cloud3dots.com
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 5.2.2
Stable tag: 5.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enables the FAMCode font set as icons within WordPress. Icons can be inserted using either HTML or a shortcode.

== Description ==
Its now very easy to integrate FAMCode font set as icons to your WordPress site. Just install this plugin and you are ready to go. Icons can be inserted using either HTML Code or Shortcode.

To use any of the font icons by Cloud3Dots on your WordPress site you have two options:

__HTML__

To add a Skype icon, you can use either of the two codes,

`<span class="fam-square-compass"></span>`

`<span data-icon="&#xe0a2;"></span>`

You can see the class names and unicode data values on the FAMCode font release page: <>

__Shortcode__

You can easily use a shortcode in your posts, pages and even widgets to display an icon. The shortcode format is [icon name=name-of-icon], where name=X is the class of the icon you would like to use.

To add a Skype icon, you can use the following shortcode,

`[icon name=fam-square-compass]`

You can see all available icon classed on the link referred above.

__Author__

*   [Cloud3Dots](http://www.cloud3dots.com)

== Installation ==

1. Upload the plugin to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
4. Use HTML by adding the appropiate class to the `<span>` element OR Add shortcode to your posts, pages or widgets to display the font icon.

== Screenshots ==


== Changelog ==

= 0.1.0 =

* Initial and stable release.

== Credits ==

* Uses FAM-Code Font https://www.dafont.com/fam-code.font & https://www.fontpalace.com/font-details/FAM-Code/, Copyright (c) Peter W. Pedrotti, [Freeware] 100%  Free for Personal and Commercial Use
