<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       cloud3dots.com
 * @since      0.1.0
 *
 * @package    Icons_Famcode
 * @subpackage Icons_Famcode/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
