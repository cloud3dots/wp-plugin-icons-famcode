<?php

/**
 * Fired during plugin deactivation
 *
 * @link       cloud3dots.com
 * @since      0.1.0
 *
 * @package    Icons_Famcode
 * @subpackage Icons_Famcode/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.1.0
 * @package    Icons_Famcode
 * @subpackage Icons_Famcode/includes
 * @author     cloud3dots <cloud3dots@gmail.com>
 */
class Icons_Famcode_Deactivator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    0.1.0
     */
    public static function deactivate()
    {
    }
}
