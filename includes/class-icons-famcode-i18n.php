<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       cloud3dots.com
 * @since      0.1.0
 *
 * @package    Icons_Famcode
 * @subpackage Icons_Famcode/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.1.0
 * @package    Icons_Famcode
 * @subpackage Icons_Famcode/includes
 * @author     cloud3dots <cloud3dots@gmail.com>
 */
class Icons_Famcode_i18n
{


    /**
     * Load the plugin text domain for translation.
     *
     * @since    0.1.0
     */
    public function load_plugin_textdomain()
    {
        load_plugin_textdomain(
            'icons-famcode',
            false,
            dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
        );
    }
}
